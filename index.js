const fs = require('fs')
const SlackWebhook = require('slack-webhook')
const Parser = require('rss-parser')
const cheerio = require('cheerio')
const config = require('./config')
const cron = require('node-cron')
const parser = new Parser()
const slack = new SlackWebhook(config.webhook)

const DB_FILE = 'cache.json'
const TYPE_BLACKLIST = ['dlc','membership','credit','other']
const re = /\[(.+)\]\s*\((.+)\)\s*(.+)/

let db = []
if (fs.existsSync(DB_FILE)) {
  db = JSON.parse(fs.readFileSync(DB_FILE))
}

function postInfo(title) {
  const info = re.exec(title)

  if (info == null) {
    return undefined
  }

  return {
    drm: info[1],
    type: info[2],
    name: info[3]
  }
}

async function run() {
  const feed = await parser.parseURL('https://www.reddit.com/r/FreeGameFindings.rss')
 
  const games = feed.items.filter(item => {
    return !db.includes(item.id)
  })

  if (games.length == 0) return

  console.log(`Found ${games.length} free game/s`)

  for (let game of games) {
    const info = postInfo(game.title)

    if (!info) continue
    if (TYPE_BLACKLIST.includes(info.type.toLowerCase())) continue

    const $ = cheerio.load(game.content)
    let url = game.link

    console.log(info.name)

    const link = $('a').filter(function() {
      return $(this).text().trim() === '[link]'
    }).first()

    if(link)
      url = link.attr('href')

    slack.send(`<${url}|Claim here!> - [${info.drm}] (${info.type}) ${info.name}`)

    db.push(game.id)
    fs.writeFileSync(DB_FILE, JSON.stringify(db))
  }
}

cron.schedule('0 * * * *', () => {
  run()
})
